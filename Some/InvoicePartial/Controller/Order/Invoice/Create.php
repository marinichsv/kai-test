<?php

namespace Some\InvoicePartial\Controller\Order\Invoice;

use Some\Model\Customer\Access\Allow;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\ResourceModel\Order as OrderResource;
use Magento\Sales\Model\ResourceModel\Order\Invoice as InvoiceResource;
use Magento\Framework\Registry;
use Magento\Framework\DB\TransactionFactory;
use Magento\Framework\App\Request\Http;

/**
 *
 */
class Create implements HttpGetActionInterface
{
    /**
     * @var Http
     */
    private $request;

    /**
     * @var InvoiceService
     */
    private $invoiceService;

    /**
     * @var OrderFactory
     */
    private $orderFactory;

    /**
     * @var OrderResource
     */
    private $orderResource;

    /**
     * @var InvoiceResource
     */
    private $invoiceResource;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var TransactionFactory
     */
    private $transactionFactory;

    /**
     * @var RawFactory
     */
    private $rawFactory;

    /**
     * @var Allow
     */
    private $allow;

    /**
     * @param Http $request
     * @param InvoiceService $invoiceService
     * @param OrderFactory $orderFactory
     * @param OrderResource $orderResource
     * @param InvoiceResource $invoiceResource
     * @param Registry $registry
     * @param TransactionFactory $transactionFactory
     * @param RawFactory $rawFactory
     * @param Allow $allow
     */
    public function __construct(
        Http $request,
        InvoiceService $invoiceService,
        OrderFactory $orderFactory,
        OrderResource $orderResource,
        InvoiceResource $invoiceResource,
        Registry $registry,
        TransactionFactory $transactionFactory,
        RawFactory $rawFactory,
        Allow $allow
    )
    {
        $this->request = $request;
        $this->invoiceService = $invoiceService;
        $this->orderFactory = $orderFactory;
        $this->rawFactory = $rawFactory;
        $this->orderResource = $orderResource;
        $this->registry = $registry;
        $this->transactionFactory = $transactionFactory;
        $this->invoiceResource = $invoiceResource;
        $this->allow = $allow;
    }

    /**
     * @return ResponseInterface|Raw|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $result = $this->rawFactory->create();

        try {

            if(!$this->allow->isAllowed()) {
                throw new LocalizedException(__("Permission denied"));
            }

            $orderId = $this->request->getParam('order_id', 4);
            $orderItemId = $this->request->getParam('order_item_id', 5);
            $amount = $this->request->getParam('amount', 40);
            $qty = $this->request->getParam('qty', '0.25');

            $order = $this->orderFactory->create();
            $this->orderResource->load($order, $orderId, $this->orderResource->getIdFieldName());

            if ($amount > 0) {
                // amount allow only if order_item::is_qty_decimal column is set to 1
                $item = $order->getItems()[$orderItemId];
                $qty = $amount / $item->getBasePrice();
                $qty = round($qty, 4);
                /// calculation should be enhanced
                ///  - with discount value
                ///  - with discount tax compensation amounts
                ///
            }

            // code from \Magento\Sales\Controller\Adminhtml\Order\Invoice\Save
            // maybe could be refactored later:
            // - possible extends from \Magento\Sales\Controller\Adminhtml\Order\Invoice\Save
            //      - need to resolve BackendAction permission code
            //      - need to resolve Post interface

            if (!$order->canInvoice()) {
                throw new LocalizedException(
                    __('The order does not allow an invoice to be created.')
                );
            }

            $invoice = $this->invoiceService->prepareInvoice($order, [ $orderItemId => "{$qty}"]);



            if (!$invoice) {
                throw new LocalizedException(__("The invoice can't be saved at this time. Please try again later."));
            }

            if (!$invoice->getTotalQty()) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __("The invoice can't be created without products. Add products and try again.")
                );
            }
            $this->registry->register('current_invoice', $invoice);
            if (!empty($data['capture_case'])) {
                $invoice->setRequestedCaptureCase($data['capture_case']);
            }

            if (!empty($data['comment_text'])) {
                $invoice->addComment(
                    $data['comment_text'],
                    isset($data['comment_customer_notify']),
                    isset($data['is_visible_on_front'])
                );

                $invoice->setCustomerNote($data['comment_text']);
                $invoice->setCustomerNoteNotify(isset($data['comment_customer_notify']));
            }

            $invoice->register();

            $invoice->getOrder()->setCustomerNoteNotify(!empty($data['send_email']));
            $invoice->getOrder()->setIsInProcess(true);

           $transactionSave = $this->transactionFactory->create()
               ->addObject($invoice)
               ->addObject($invoice->getOrder())
           ;

            $transactionSave->save();

            $result->setContents("Done!, New Invoice was created : {$invoice->getIncrementId()}");
        } catch (\Exception $ex) {
            $result->setContents("{$ex->getMessage()} : {$ex->getTraceAsString()}");
        }

        return $result;
    }
}
